package org.bitbucket.juanmougan.mycar

import android.app.Application
import io.paperdb.Paper

/**
 * Created by juanma on 24/6/18.
 */
class CarApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Paper.init(this.applicationContext)
    }
}