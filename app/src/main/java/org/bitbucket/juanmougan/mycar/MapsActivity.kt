package org.bitbucket.juanmougan.mycar

import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private var mLocationPermissionGranted: Boolean = false
    private lateinit var mFusedLocationProviderClient: FusedLocationProviderClient

    // A default location (Buenos Aires, Argentina) and default zoom to use when location permission is
    // not granted.
    private val mDefaultLocation = LatLng(-34.6083667,-58.3744719)
    // Default value for Map zoom
    private val DEFAULT_ZOOM = 15
    private val LOCATION_KEY = "CarLocation"

    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    // TODO retrieve from DB
    private var mLastKnownLocation: Location? = null

    private val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        // Try to retrieve the car's location from SharedPreferences
        val sharedPref = getPreferences(Context.MODE_PRIVATE)
        val gson = Gson()
        val json = sharedPref.getString(LOCATION_KEY, "")
        if (json != null && !json.isEmpty()) {
            // Retrieving location from parked car
            mLastKnownLocation = gson.fromJson(json, Location::class.java)
        } else {
            // Car is not parked
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI()

        // Get the current location of the device and set the position of the map.
        getDeviceLocation()
    }

    private fun saveLocationToPrefs() {
        val sharedPref = getPreferences(Context.MODE_PRIVATE)
        val prefsEditor = sharedPref.edit()
        val gson = Gson()
        val json = gson.toJson(mLastKnownLocation)
        prefsEditor.putString(LOCATION_KEY, json)
        prefsEditor.apply()
    }

    /**
     *
     * Request location permission, so that we can get the location of the
     * device. The result of the permission request is handled by a callback,
     * onRequestPermissionsResult.
     */
    private fun getLocationPermission() {
        if (ContextCompat.checkSelfPermission(this.applicationContext,
                android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true
        } else {
            ActivityCompat.requestPermissions(this,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        }
    }

    /**
     * Handles the result of the permission request.
     */
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        mLocationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true
                }
            }
        }
        updateLocationUI()
    }

    /**
     * Updates the map's UI settings based on whether the user has granted location permission.
     */
    private fun updateLocationUI() {
        if (mMap == null) {
            return
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.isMyLocationEnabled = true
                mMap.uiSettings.isMyLocationButtonEnabled = true
            } else {
                mMap.isMyLocationEnabled = false
                mMap.uiSettings.isMyLocationButtonEnabled = false
                mLastKnownLocation = null
                getLocationPermission()
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message)
        }

    }

    private fun getDeviceLocation() {
        /*
     * Get the best and most recent location of the device, which may be null in rare
     * cases when a location is not available.
     */
        try {
            if (mLocationPermissionGranted) {
                val locationResult = mFusedLocationProviderClient.lastLocation
                locationResult.addOnCompleteListener(this, {
                    if (it.isSuccessful && it.result != null) {
                        // Set the map's camera position to the current location of the device.
                        mLastKnownLocation = it.result
                        val currentLocation = LatLng(mLastKnownLocation!!.latitude,
                                mLastKnownLocation!!.longitude)
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                currentLocation, DEFAULT_ZOOM.toFloat()))
                        // Add a marker in the current position
                        mMap.addMarker(MarkerOptions()
                                .title(getString(R.string.car_location))
                                .position(currentLocation))
                    } else {
                        Log.d("MapsActivity", "Current location is null. Using defaults.")
                        Log.e("MapsActivity", "Exception: %s", it.getException())
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM.toFloat()))
                        mMap.uiSettings.isMyLocationButtonEnabled = false
                        val locationErrorToast = Toast.makeText(this, R.string.location_error, Toast.LENGTH_SHORT)
                        val tv = locationErrorToast.view.findViewById<View>(android.R.id.message) as TextView
                        tv.gravity = Gravity.CENTER
                        locationErrorToast.show()
                    }
                })
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message)
        }

    }

}
